#!/bin/bash

buildPlatformWithCache()
{
mkdir -p $1
pushd $1

getGodot "https://github.com/godotengine/godot.git" "master"
getModule "https://github.com/Zylann/godot_voxel.git" "master" "voxel"
getModule "https://gitlab.com/greenfox/double-pass-streamer.git" "master" "double_pass_streamer"


compileGodot $1

popd
}

buildPlatformWithCache linux
buildPlatformWithCache windows
buildPlatformWithCache core


mkdir -p bin/
pushd bin

cp ../*/godot/bin/* ../*/bin/* .

popd

ls bin

buildHTML bin


chown --reference=buildVoxel.sh * -R
